module bitbucket.org/_metalogic_/logs

go 1.15

require (
	bitbucket.org/_metalogic_/log v1.4.1
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
	gopkg.in/yaml.v2 v2.2.7
)
