package logs

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"plugin"
	"strings"

	"bitbucket.org/_metalogic_/log"
)

var (
	loggers map[string]Logger
)

func init() {
	loggers = make(map[string]Logger)
}

// Logger defines the type for conversion from platform-specific log formats to
// the CommonLog structure defined by the logs package
type Logger interface {
	Common(Record) (CommonLog, error)
}

// RegisterAdapters registers log adapters from directory
func RegisterAdapters(dir string) error {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return err
	}
	for _, f := range files {
		name := f.Name()
		if strings.HasSuffix(name, ".so") {
			log.Debugf("Registering log adapter '%s'", name)
			path := fmt.Sprintf("%s%c%s", dir, filepath.Separator, name)
			if err = register(name, path); err != nil {
				log.Fatal(err)
			}
		}
	}
	return nil
}

func register(name, path string) error {
	// open the so file to load the symbols
	plug, err := plugin.Open(path)
	if err != nil {
		return err
	}
	// look up exported symbol Logger
	sym, err := plug.Lookup("Logger")
	if err != nil {
		return err
	}

	// assert that loaded symbol is of a desired type Logger
	logger, ok := sym.(Logger)
	if !ok {
		return fmt.Errorf("unexpected type from module symbol")
	}
	loggers[name] = logger
	return nil
}
