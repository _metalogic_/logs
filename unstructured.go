package logs

var defaultLogger = &logback{}

type logback struct{}

// Common returns CommonLog converted from record
func (l logback) Common(record Record) (common CommonLog, err error) {
	common.Logger = "default"
	common.Message = record.Message
	common.Level = Debug
	return common, err
}
