package logs

import (
	"fmt"
	"regexp"
	"strings"

	"bitbucket.org/_metalogic_/log"
)

var (
	filter = &Filter{}
)

// Filter defines properties used to match log records for inclusion in output
type Filter struct {
	logLevel  LogLevel            `json:"logLevel,omitempty"`
	grep      string              `json:"grep,omitempty"`
	xgrep     string              `json:"xgrep,omitempty"`
	services  map[string][]string `json:"services,omitempty"`
	xservices map[string][]string `json:"xservices,omitempty"`
}

// NewFilter initializes an empty filter
func NewFilter() (filter *Filter) {
	filter = new(Filter)
	filter.services = make(map[string][]string)
	filter.xservices = make(map[string][]string)
	return filter
}

func (filter *Filter) matches(l CommonLog) bool {
	// if verbose is true don't filter out omitted records
	if !config.verbose && l.omit() {
		log.Debug("filter out omitted record")
		return false
	}
	if len(filter.xservices) > 0 && filter.matchService(l.Service, l.Component, filter.xservices) {
		log.Debug("filter out excluded service")
		return false
	}
	if len(filter.services) > 0 && !filter.matchService(l.Service, l.Component, filter.services) {
		log.Debug("filter out unmatched service")
		return false
	}
	if filter.logLevel != "" && !filter.matchLevel(filter.logLevel, l.Level) {
		log.Debug("filter out unmatched log level")
		return false
	}
	if filter.grep != "" && !filter.matchGrep(filter.grep, l.Message) {
		log.Debug("filter out unmatched grep")
		return false
	}
	if filter.xgrep != "" && filter.matchGrep(filter.xgrep, l.Message) {
		log.Debug("filter out matched xgrep")
		return false
	}
	return true
}

func (filter *Filter) setService(serviceName, component string) (err error) {
	var service *Service
	if service := config.Service(serviceName); service == nil {
		return fmt.Errorf("service '%s' not found", serviceName)
	}
	matches, ok := filter.services[serviceName]
	if !ok {
		matches = []string{}
		filter.services[serviceName] = matches
	}
	if component != "" {
		found := false
		for _, c := range service.Components {
			if c == component {
				found = true
				matches = append(matches, component)
				filter.services[serviceName] = matches
				break
			}
		}
		if !found {
			return fmt.Errorf("invalid '%s' is not a component of %s", component, serviceName)
		}
	}
	return nil
}

func (filter *Filter) matchService(service string, component string, services map[string][]string) bool {
	if components, ok := services[service]; ok {
		if len(components) == 0 { // only filter on source
			return true
		}
		if component == "" {
			return false
		}
		for _, c := range components {
			if c == component {
				return true
			}
		}
	}
	return false
}

func (filter *Filter) matchLevel(filterLevel, recordLevel LogLevel) bool {
	var filterIndex, recordIndex int
	for i, l := range LogLevels {
		if l == filterLevel {
			filterIndex = i + 1
		}
		if l == recordLevel {
			recordIndex = i + 1
		}
	}
	return (recordIndex >= filterIndex || (config.verbose && recordLevel == None))
}

func (filter *Filter) matchGrep(grep, message string) bool {
	return strings.Contains(message, grep)
}

func (filter *Filter) filters() string {
	var str string
	if filter.logLevel != "" {
		str = fmt.Sprintf("Log Level:\n\t%s\n", filter.logLevel)
	}
	if filter.grep != "" {
		str = fmt.Sprintf("Grep:\n\t%s\n", filter.grep)
	}
	if len(filter.services) > 0 {
		str = str + "services:\n"
		for source, components := range filter.services {
			str = str + fmt.Sprintf("\t%s/%v\n", source, components)
		}
	}
	if str != "" {
		str = "---\n" + str
	}
	return str
}

// Filters ...
func Filters() string {
	return filter.filters()
}

// SetGrep ...
func SetGrep(grep string) {
	filter.grep = grep
}

// SetXGrep ...
func SetXGrep(grep string) {
	filter.xgrep = grep
}

// SetLevel ...
func SetLevel(level LogLevel) {
	filter.logLevel = level
}

// SetServices ...
func SetServices(serviceFlgs []string, exclude bool) error {
	services := make(map[string][]string)
	var component string
	for _, v := range serviceFlgs {
		// a service flag may be a service name or service name/component
		parts := strings.Split(v, "/")
		serviceName := parts[0]
		var service = config.Service(serviceName)
		if service == nil {
			return fmt.Errorf("service '%s' is not configured", serviceName)
		}
		matches, ok := services[serviceName]
		if !ok {
			matches = []string{}
			services[serviceName] = matches
		}
		if len(parts) == 2 {
			component = parts[1]

			found := false
			for _, c := range service.Components {
				if c == component {
					found = true
					matches = append(matches, component)
					services[serviceName] = matches
					break
				}
			}
			if !found {
				return fmt.Errorf("invalid component '%s' is not a member of %s/%s", component, serviceName, service.Components)
			}
		}
	}
	if exclude {
		filter.xservices = services
	} else {
		filter.services = services
	}
	return nil
}

// Matches ...
func Matches(l CommonLog) bool {
	return filter.matches(l)
}

// RegexpMatch returns bool indicating if text matches pattern
func RegexpMatch(text, pattern string) bool {
	re, err := regexp.Compile(".*" + pattern + ".*")
	if err != nil {
		log.Error(err)
		return false
	}
	return re.MatchString(text)
}
