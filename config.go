package logs

import (
	"fmt"
	"time"

	"gopkg.in/yaml.v2"
)

const timeLayout = "2006-01-02 15:04:05 MST"

var (
	config = &Config{}
)

// Config captures logs configuration
type Config struct {
	location   *time.Location
	timeLayout string
	debug      bool
	verbose    bool
	Services   map[string]*Service `yaml:"Services"`
}

func (c *Config) Service(name string) *Service {
	for service := range c.Services {
		if service == name {
			return c.Services[name]
		}
	}
	return nil
}

// Service defines log details for a service running in Docker Swarm
type Service struct {
	Logger     string   `yaml:"logger"`
	Omits      []string `yaml:"omits,omitempty"`
	Components []string `yaml:"components,omitempty"`
}

// Configure ...
func Configure(data []byte) (*Config, error) {
	if err := yaml.Unmarshal(data, &config); err != nil {
		return config, err
	}
	config.timeLayout = timeLayout
	return config, nil
}

// SetDebug ...
func SetDebug(debug bool) {
	config.debug = debug
}

// SetLocation ...
func SetLocation(location *time.Location) {
	config.location = location
}

// SetVerbose ...
func SetVerbose(verbose bool) {
	config.verbose = verbose
}

// PrintConfig ...
func PrintConfig() {
	for k := range config.Services {
		fmt.Printf("%s: %v+\n", k, config.Services[k])
	}
	for k := range loggers {
		fmt.Printf("logger: %s\n", k)
	}
}
