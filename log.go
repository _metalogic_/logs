package logs

import (
	"crypto/md5"
	"fmt"
	"strings"
	"time"

	"gopkg.in/yaml.v2"
)

// LogLevel is an enumeration of log level names
type LogLevel string

// enumeration of log levels as strings
const (
	None  LogLevel = "none"
	Trace          = "trace"
	Debug          = "debug"
	Info           = "info"
	Warn           = "warn"
	Error          = "error"
	Fatal          = "fatal"
	Any            = "any"
)

// LogLevels defines the ordering of log levels used in deciding to emit a log record;
// logs are emitted for a given level and all levels that follow in the ordering
var LogLevels = []LogLevel{None, Trace, Debug, Info, Warn, Error, Fatal, Any}

// String and Set are required to implement the flag.Value interface
func (f LogLevel) String() string {
	return string(f)
}

// Set log level according to given value string
func (f *LogLevel) Set(value string) error {
	for _, level := range LogLevels {
		if string(level) == value {
			*f = LogLevel(value)
			return nil
		}
	}
	return fmt.Errorf("level must be one of %v", LogLevels)
}

// Log holds a log returned in a lar stream or query response
type Log struct {
	Timestamp int64  `json:"time"`
	Record    Record `json:"record"` // TODO change record to data in output from lard
}

// Record ...
type Record struct {
	DateTime time.Time              `json:"@timestamp" yaml:"datetime"`
	Host     string                 `json:"host"`
	Message  string                 `json:"message"`
	Docker   Docker                 `json:"docker"`
	Event    map[string]interface{} `json:"event"`
}

// Docker defines Docker details exported by logspout
type Docker struct {
	Name        string            `json:"name"`
	ContainerID string            `json:"cid"`
	Image       string            `json:"image"`
	Tag         string            `json:"image_tag"`
	Source      string            `json:"source"`
	Labels      map[string]string `json:"labels"`
}

// CommonLog is the target format for log adapters
type CommonLog struct {
	Service   string                 `json:"service" yaml:"service"`
	Component string                 `json:"component,omitempty" yaml:"component,omitempty"`
	Datetime  time.Time              `json:"datetime" yaml:"datetime"`
	Localtime time.Time              `json:"localtime" yaml:"localtime"`
	Level     LogLevel               `json:"level" yaml:"level"`
	Message   string                 `json:"message" yaml:"message"`
	Context   string                 `json:"context,omitempty" yaml:"context,omitempty"`
	Fields    map[string]interface{} `json:"fields,omitempty" yaml:"-"`
	Logger    string                 `json:"logger" yaml:"logger"`
	Timestamp int64                  `json:"timestamp" yaml:"timestamp"`
}

func (l CommonLog) omit() bool {
	if strings.Replace(l.Message, " ", "", -1) == "" {
		return true
	}
	service := config.Service(l.Service)
	if service == nil {
		return false
	}
	for _, pattern := range service.Omits {
		if RegexpMatch(l.Message, pattern) {
			return true
		}
	}
	return false
}

func (l CommonLog) hash() string {
	return fmt.Sprintf("%x", md5.Sum([]byte(l.Message)))
}

// MarshalYAML ...
func (l CommonLog) MarshalYAML() (string, error) {
	yaml, err := yaml.Marshal(l)
	return string(yaml), err
}
