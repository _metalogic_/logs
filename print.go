package logs

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"time"

	"bitbucket.org/_metalogic_/log"
	"golang.org/x/crypto/ssh/terminal"
	"gopkg.in/yaml.v2"
)

// max length of a scan line
const maxCapacity = 1024 * 1024

var (
	stdout io.ReadWriter = os.Stdout
	sep                  = []byte("---\n")
	eol                  = []byte("\n")
)

// Print prints input from reader as YAML or JSON if raw is true
func Print(reader io.Reader, raw bool) error {

	term := terminal.NewTerminal(stdout, ":")

	scanner := bufio.NewScanner(reader)

	// use custom scan buffer
	buf := make([]byte, maxCapacity)
	scanner.Buffer(buf, maxCapacity)

	for scanner.Scan() {
		var logEntry Log
		data := scanner.Bytes()
		err := json.Unmarshal(data, &logEntry)
		if err != nil { // ignore JSON unmarshall errors
			continue
		}
		common, err := commonLog(logEntry)
		log.Debugf("Unmarshalled logentry: %+v to %+v", logEntry, common)
		if err != nil {
			log.Error(err)
			continue
		}
		if !Matches(common) {
			continue
		}

		if raw {
			term.Write(data)
			term.Write(eol)
			continue
		}
		printYAML(common, term)
	}
	if err := scanner.Err(); err != nil {
		log.Fatalf("error: %s", err)
	}

	return nil
}

func printYAML(common CommonLog, term *terminal.Terminal) {
	log.Debugf("printing: %+v", common)
	yml, err := yaml.Marshal(common)
	if err != nil {
		term.Write(term.Escape.Blue)
		term.Write(sep)
		term.Write([]byte(fmt.Sprintf("%s\n", err)))
		return
	}
	switch common.Level {
	case Error:
		term.Write(term.Escape.Red)
	case Warn:
		term.Write(term.Escape.Yellow)
	}
	defer term.Write(term.Escape.Reset)
	term.Write(sep)
	term.Write(yml)
}

func commonLog(logentry Log) (common CommonLog, err error) {
	record := logentry.Record
	namespace := record.Docker.Labels["com.docker.stack.namespace"]
	serviceName := record.Docker.Labels["com.docker.swarm.service.name"][len(namespace)+1:]
	log.Debugf("getting common log for service: %s", serviceName)
	service := config.Service(serviceName)
	if service == nil { // service is not configured - use default logger
		return defaultLogger.Common(record)
	}
	if logger, ok := loggers[service.Logger]; ok {
		if common, err = logger.Common(record); err != nil {
			return common, err
		}
		common.Service = serviceName
		common.Datetime = record.DateTime
		common.Localtime = timeIn(common.Datetime, "Canada/Pacific")
		common.Timestamp = logentry.Timestamp

		return common, err
	}
	return common, fmt.Errorf("no logger registered for '%s'", serviceName)
}

func timeIn(utc time.Time, zone string) time.Time {
	loc, err := time.LoadLocation(zone)
	if err != nil {
		panic(err)
	}
	return utc.In(loc)
}
